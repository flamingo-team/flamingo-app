import _ from 'lodash';
import React, { useEffect, useContext } from 'react';
import { Container, Header, ScrollableTab, Tab, Tabs, Title, Body } from 'native-base';
import Tab1 from './Assets';
import Tab2 from './Assets';
import Tab3 from './Assets';
import Tab4 from './Assets';
const LazyCoins = React.lazy((): any => import('../components/AssetsList'));

import { Store } from '../store';

import { INavProps, ICoin, IWallet } from '../store/interfaces';
import { LoadingSpinner } from '../components';
import { API, Colors, IS_ANDROID } from '../enums';
import { fetchCoinListAction } from '../store/actions';
import { InteractionManager, StatusBar } from 'react-native';
import { NavigationEventSubscription } from 'react-navigation';

const Prices: React.FC<INavProps> = props => {
  const {
    state: { coins, wallets },
    dispatch,
  } = useContext(Store);
  // define local state to hold the values of page for list pagination purpose
  const [page, setPage] = React.useState<number>(1); // default is 1
  // rearrange coins and wallets to see which coins are tradable.
  const [assets, setAssets] = React.useState<ICoin[]>([]);
  const [isAssetLoading, setLoading] = React.useState<boolean>(true);
  //navigate to coin details.
  const navigateToDetail = (coin: ICoin): boolean => props.navigation.navigate('Coin', { coin });
  //fetch pagination
  const handleLoadMore = (): void => {
    if (page < API.MAXIMUM_FETCH / API.LIMIT && coins.length <= page * API.LIMIT && coins.length < API.MAXIMUM_FETCH) {
      // making sure we don't load the same data twice.
      fetchCoinListAction(API.LIMIT, page, dispatch);
      setPage(page + 1);
    }
  };


  useEffect(() => {
    InteractionManager.runAfterInteractions(async () => {
      const arrangedCoins = coins.map((c: ICoin) =>
        wallets.find((w: IWallet): boolean => w.symbol === c.symbol)
          ? { ...c, tradable: true }
          : { ...c, tradable: false },
      );
      await setAssets(arrangedCoins);
      setLoading(false);
    });
    
    const sbListener: NavigationEventSubscription = props.navigation.addListener('didFocus', (): void => {
      StatusBar && StatusBar.setBarStyle('dark-content');
      IS_ANDROID && StatusBar && StatusBar.setBackgroundColor(Colors.black);
    });
    return (): void => sbListener.remove(); // cleanup
  }, []);

  const sendtransaction = (transactionid,amount) => {

fetch(`http://34.77.195.149/transaction/${transactionid}/authorize`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                  amount: amount,
                  currency: "php"
            })
        })

            .then((response) => response.json())
            .then((responseData) => {
                console.log("RESULTS HERE:", responseData)

        //     this.setState({
        //   isLoading: false,
        //   dataSource: responseJson,
        // }, function(){

        // });
      })
      .catch((error) =>{
        console.error(error);
      }) 
};

  
  return (
    <Container>
      <Header
        androidStatusBarColor={Colors.black}
        iosBarStyle={'dark-content'}
        hasTabs
        transparent
        translucent
        hasSubtitle={false}
        style={{ backgroundColor: Colors.gray + '60' }}>
        <Body>
          <Title style={{ color: Colors.black }}>Prices</Title>
        </Body>
      </Header>
      {!isAssetLoading && coins.length ? (
        <Tabs
          tabContainerStyle={{ backgroundColor: Colors.lightGray }}
          renderTabBar={() => (
            <ScrollableTab underlineStyle={{ backgroundColor: Colors.black }} backgroundColor={Colors.lightGray} />
          )}>
          <Tab
            tabStyle={{ backgroundColor: Colors.lightGray }}
            activeTabStyle={{ backgroundColor: Colors.lightGray }}
            activeTextStyle={{ color: Colors.black }}
            textStyle={{ color: Colors.black + '50' }}
            heading="Restest">
            <React.Suspense fallback={<LoadingSpinner />}>
              <LazyCoins withAction={false} items={assets} onPress={sendtransaction} handleLoadMore={handleLoadMore} />
            </React.Suspense>
          </Tab>
          <Tab
            tabStyle={{ backgroundColor: Colors.lightGray }}
            activeTabStyle={{ backgroundColor: Colors.lightGray }}
            activeTextStyle={{ color: Colors.black }}
            textStyle={{ color: Colors.black + '50' }}
            heading="requested">
            <React.Suspense fallback={<LoadingSpinner />}>
              <LazyCoins
                withAction={false}
                items={assets.filter((c: ICoin): boolean => c.tradable === true)}
                onPress={sendtransaction}
                handleLoadMore={handleLoadMore}
              />
            </React.Suspense>
          </Tab>
     
      
        </Tabs>
      ) : (
        <LoadingSpinner />
      )}
    </Container>
  );
};

export default Prices;
