import React, { useContext } from 'react';
import { StatusBar } from 'react-native';
import { INavProps, IInvestInfo } from '../store/interfaces';
import { Invest } from '../components';
import { transactAction } from '../store/actions';
import { Colors, Transact, IS_ANDROID } from '../enums';
import { Store } from '../store';
import { NavigationEventSubscription } from 'react-navigation';
import { Container } from 'native-base';

const TransactScreen: React.FC<INavProps> = ({ navigation }) => {
  // get the state data using react hooks (useContext)
  const {
    state: { coins, wallets },
    dispatch,
  } = useContext(Store);
  const singleWallet = navigation.getParam('wallet', null);
  // handle sell button press ==> called from the modal
  const onSellBtnPress = (data: IInvestInfo): void => {
    transactAction(data, Transact.SELL, wallets, dispatch);
  };
  // handle buy button press ==> called from the modal
  const onBuyBtnPress = (data: IInvestInfo): void => {
    transactAction(data, Transact.BUY, wallets, dispatch);
  };
  // goBack to the first screen in the stack
  const navigateToHome = (): boolean => {
    if (singleWallet) {
      return navigation.goBack(); // going back to the coin we originally navigate from
    } else {
      return navigation.navigate('AccountsStack');
    }
  };

  // make sure status bar always have a light color
  React.useEffect((): any => {
    const txListener: NavigationEventSubscription = navigation.addListener('didFocus', (): void => {
      StatusBar && StatusBar.setBarStyle('light-content');
      IS_ANDROID && StatusBar && StatusBar.setBackgroundColor(Colors.white);
    });
    return (): void => txListener.remove(); // cleanup
  }, []);

  return (
    <Container>
      <StatusBar barStyle="light-content" backgroundColor={Colors.white} />
      <Invest
        wallets={singleWallet ? singleWallet : wallets}
        onClose={navigateToHome}
        onBuy={onBuyBtnPress}
        onSell={onSellBtnPress}
      />
    </Container>
  );
};

export default TransactScreen;
