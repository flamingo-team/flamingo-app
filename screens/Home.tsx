import React, { useContext } from 'react';
import { StyleSheet, StatusBar, SafeAreaView, Platform, InteractionManager } from 'react-native';
import * as WebBrowser from 'expo-web-browser';
import { Store } from '../store';
import { INavProps, ICoin, INews, IInvestInfo } from '../store/interfaces';
import {
  fetchCoinListAction,
  fetchLatestNewsAction,
  transactAction,
  updateAllPricesAction,
  fetchTopListtAction,
} from '../store/actions';
import { Colors, Transact, IS_ANDROID } from '../enums';
import { Main } from '../components';
import { NavigationEventSubscription } from 'react-navigation';
import { Header } from 'native-base';

const Home = ({ navigation }: INavProps): React.ReactElement<any> => {
  // get the state data using react hooks (useContext)
  const {
    state: { favourites, news, wallets, coins, isLoading, toplist },
    dispatch,
  } = useContext(Store);
  // fetch news from news action when the component mounted using useEffect hook
  React.useEffect((): any => {
    const homeNavListener: NavigationEventSubscription = navigation.addListener('didFocus', (): void => {
      StatusBar.setBarStyle('light-content');
      Platform.OS === 'android' && StatusBar.setBackgroundColor(Colors.white);
    });
    // run the fetching after navigation completely animate.
    InteractionManager.runAfterInteractions(() => {
      fetchLatestNewsAction(dispatch);
      fetchTopListtAction(dispatch);
      fetchCoinListAction(20, 0, dispatch);
    });

    return (): void => homeNavListener.remove(); // cleanup
  }, []);
  // navigate to the selected coin
  const populateCoin = (coin: ICoin): boolean => {
    //setCurrentCoin(coin, dispatch); // set current coin state
    return navigateToScreen('Coin', { coin });
  };
  // generic nav to any screen based on the screen name
  const navigateToScreen = (screen: string, params?: any): boolean => {
    return navigation.navigate(screen, params);
  };
  // refresh the coins and wallets data
  const onRefresh = (): void => {
    fetchCoinListAction(20, 0, dispatch); //get all coins
  };
  // refresh favouirte coins prices
  const onRefreshPrices = (): void => {
    if (coins.length) updateAllPricesAction(coins, dispatch);

    if (toplist.length) fetchTopListtAction(dispatch);
  };
  //handle pressing on the a news item
  const onNewsPress = async (news: INews): Promise<any> => {
    await WebBrowser.openBrowserAsync(news.url);
  };
  // handle sell button press ==> called from the modal
  const onSellBtnPress = (data: IInvestInfo): void => {
    transactAction(data, Transact.SELL, wallets, dispatch);
  };
  // handle buy button press ==> called from the modal
  const onBuyBtnPress = (data: IInvestInfo): void => {
    transactAction(data, Transact.BUY, wallets, dispatch);
  };
  
  getRemoteData = () => {

fetch('http://34.77.195.149/users/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: 'giraffe@example.com',
                password: 'extrasecure',
            })
        })

            .then((response) => response.json())
            .then((responseData) => {
                console.log("RESULTS HERE:", responseData)

            this.setState({
          isLoading: false,
          dataSource: responseJson,
        }, function(){

        });
      })
      .catch((error) =>{
        console.error(error);
      }) 
};
  return (
    <SafeAreaView style={styles.container}>
      {/* <StatusBar backgroundColor={Colors.white} barStyle="light-content" /> */}
      {IS_ANDROID ? (
        <Header
          androidStatusBarColor={Colors.white}
          iosBarStyle={'light-content'}
          transparent
          translucent
          hasTabs
          hasSubtitle={false}
          style={{ backgroundColor: Colors.blue }}></Header>
      ) : null}
      <Main
        onBuyBtnPress={onBuyBtnPress}
        onSellBtnPress={onSellBtnPress}
        onNewsPress={onNewsPress}
        onRefresh={onRefresh}
        navigateToScreen={navigateToScreen}
        populateCoin={populateCoin}
        wallets={wallets}
        toplist={toplist}
        favourites={favourites}
        news={news}
        onRefreshPrices={onRefreshPrices}
        isRefreshingPrice={isLoading}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1, // take up the whole screen
    backgroundColor: Colors.trueBlue,
  },
});
export default Home;
