import React from 'react';
import { StatusBar, StyleSheet, View, TouchableOpacity, Platform } from 'react-native';
import { Store } from '../store';
import * as WebBrowser from 'expo-web-browser';
import { INavProps, INews } from '../store/interfaces';
import { LoadingSpinner } from '../components';
import { Colors } from '../enums';
import { Header, Left, Icon, Body, Title, Right } from 'native-base';
import { NavigationEventSubscription } from 'react-navigation';
import { scale, moderateScale } from 'react-native-size-matters';
const LazyStories = React.lazy((): any => import('../components/StoriesList'));
const isAndroid = Platform.OS === 'android';

const Stories = ({ navigation }: INavProps): React.ReactElement<any> => {
  // get the state data using react hooks (useContext)
  const {
    state: { news },
  } = React.useContext(Store);
  // set statusbar styling when the component first mounted
  React.useEffect((): any => {
    const navListener: NavigationEventSubscription = navigation.addListener('didFocus', (): void => {
      StatusBar.setBarStyle('dark-content');
      isAndroid && StatusBar.setBackgroundColor(Colors.black);
    });
    return (): void => navListener.remove(); // cleanup
  }, []);
  // handling pressing on the news item by opening the link using WebBrowser
  const _handlePressNewsAsync = async (news: INews): Promise<any> => {
    await WebBrowser.openBrowserAsync(news.url);
  };
  // navigate back
  const goBack = (): boolean => navigation.goBack();
  // render header using native-base header component
  const renderHeader = (): any => (
    <Header translucent transparent>
      <Left>
        <TouchableOpacity
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            width: 50,
          }}
          onPress={goBack}>
          <Icon name={'md-arrow-back'} color={Colors.black} type="Ionicons" fontSize={scale(32)} />
        </TouchableOpacity>
      </Left>
      <Body>
        <Title style={{ color: Colors.black }}>News</Title>
      </Body>
      <Right />
    </Header>
  );
  // render component
  return (
    <View style={styles.container}>
      {renderHeader()}
      <React.Suspense fallback={<LoadingSpinner />}>
        <LazyStories items={news} onPress={_handlePressNewsAsync} />
      </React.Suspense>
    </View>
  );
};
// remove the react-navigation header
Stories.navigationOptions = {
  header: null,
};

const styles = StyleSheet.create({
  container: { backgroundColor: Colors.white, flex: 1 },
  contentContainer: {
    flex: 60,
  },
  list: {
    flexWrap: 'wrap', // allow multiple rows
    paddingHorizontal: moderateScale(10),
  },
});
export default Stories;
