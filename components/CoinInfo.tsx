import React from 'react';
import { View, StyleSheet, ScrollView, Text } from 'react-native';

import About from './About';
import MarketStats from './MarketStats';
import { Colors, Screen } from '../enums';
import { ICoin, IChartProps, IRangeSwitcherProps, IWallet } from '../store/interfaces';
import Chart from './Chart';
import LoadingSpinner from './Spinner';
import Switcher from './Switcher';
import WalletListItem from './WalletListItem';
import { Button, Icon } from 'native-base';
import { verticalScale, scale } from 'react-native-size-matters';

interface IProps {
  coin: ICoin;
  wallet: IWallet;
  chartProps: IChartProps;
  switcherProps: IRangeSwitcherProps;
  onInvestBtnPress: (...args: any) => any;
  onFollow?: (...args: any) => any;
  faved: boolean;
}

export default function CoinInfo({
  coin,
  wallet,
  chartProps,
  switcherProps,
  onInvestBtnPress,
  onFollow,
  faved,
}): React.ReactElement<IProps> {
  // dummy rendering and calls. all the logic is in the component under screens folder (screens/)
  const _onInvestBtnPress = (): void => onInvestBtnPress();

  return (
    <ScrollView style={{ backgroundColor: Colors.white }}>
      {!chartProps.isLoading ? (
        <Chart {...chartProps} range={switcherProps.current} withHeader />
      ) : (
        <LoadingSpinner containerStyle={{ height: Screen.chartHeight + verticalScale(64) }} />
      )}
      <Switcher {...switcherProps} />

      <WalletListItem
        {...wallet}
        onPress={(): void => {}}
        style={{
          backgroundColor: Colors.white,
          borderBottomWidth: 1,
          borderTopWidth: 1,
          borderColor: Colors.gray,
          height: 65,
        }}
      />

      <View style={styles.buttonContainer}>
        {wallet ? (
          <Button
            style={{ flex: 1, marginHorizontal: 10, backgroundColor: Colors.trueBlue }}
            block
            onPress={_onInvestBtnPress}>
            <Text style={styles.buttonText}>{`BUY | SELL ${coin.symbol}`}</Text>
          </Button>
        ) : (
          <Button
            iconLeft
            bordered
            style={{ flex: 1, marginHorizontal: 10, borderColor: Colors.trueBlue }}
            block
            onPress={onFollow}>
            <Icon
              name={faved ? 'star' : 'star-outline'}
              style={{ marginLeft: scale(-15), paddingHorizontal: scale(10), color: Colors.black }}
            />
            <Text style={[styles.buttonText, { color: faved ? Colors.black : Colors.trueBlue }]}>
              {faved ? 'Unfollow' : `Follow`}
            </Text>
          </Button>
        )}
      </View>
      <MarketStats coin={coin} />
      <About coin={coin} />
    </ScrollView>
  );
}
const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: verticalScale(10),
  },
  buttonText: {
    color: Colors.white,
    fontWeight: '600',
    alignSelf: 'center',
    fontSize: scale(13),
  },
});
