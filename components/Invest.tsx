import React, { useEffect } from 'react';
import numeral from 'numeral';
import { View, Text, Image, Alert, StyleSheet, Platform, Share } from 'react-native';
import {
  Header,
  Content,
  Form,
  Item,
  Picker,
  Icon,
  Left,
  Body,
  Title,
  Right,
  Button,
  Container,
  Input,
} from 'native-base';
import { Colors, Screen, IS_iOS } from '../enums';
import { IWallet } from '../store/interfaces';
import LoadingSpinner from './Spinner';
import { scale, moderateScale, verticalScale } from 'react-native-size-matters';
import Keyboard from './Keyboard';

interface IProps {
  onClose: (...args: any) => any;
  onBuy: (...args: any) => any;
  onSell: (...args: any) => any;
  wallets: IWallet[];
}
/**
 * Invest @component
 * @param props
 */
const Invest: React.FC<IProps> = ({ onClose, onBuy, onSell, wallets }) => {
  /**
  |--------------------------------------------------
  | if the wallets.length is 1 means it is coming from specific coin/asset details (from Coin.tsx).
  | otherwise the picker will represent all assets / wallets
  |--------------------------------------------------
  */
  const _initialState =
    wallets.length === 1
      ? {
          currency: wallets[0].symbol,
          input: '',
          amountInUSD: 0,
          account:'',
          icon: wallets[0].icon,
        }
      : {
          currency: wallets[0] === undefined ? null : wallets[0].symbol,
          input: '',
          amountInUSD: 0,
          account:'',
          icon: wallets[0] === undefined ? null : wallets[0].icon,
        };
  // define local state to hold the transaction data using useState hook
  const [state, setState] = React.useState(_initialState);
  // handling selecting a coin from the list/picker list
  const onCurrencyChange = (value: string): void => {
    const coin = wallets.filter((e): boolean => e.symbol === value);
    const _price: string = coin ? coin[0].marketPrice : '0';
    setState({
      ...state,
      currency: value,
      icon: coin[0].icon,
      amountInUSD: numeral(_price)._value * numeral(state.input)._value,
    });
  };

  const convertPHP_to_Pounds = (amount) =>{

    let phpamout = amount / 116;

    return phpamout;

  }
  // handling the amount imput change and update the input state
    const onInputTextChanged2 = (text: any): void => {
  

    if (text !== '.' && isNaN(text)) {
      const _input = state.input.substring(0, state.input.length - 1);
      // not a valid number
      setState({
        ...state,
        account: _input,
      });
      return;
    } else {
      setState({
        ...state,
        account: state.input + text,
      });
    }
    // _input && _input.focus(); // focus to toggle the keyboard
  };

  const onInputTextChanged = (text: any): void => {
    if (!state.currency) {
      Alert.alert('Select Coin', 'Please select coin first');
      setState({ ...state, input: '' });
      return;
    }
    const coin = wallets.filter((e): boolean => e.symbol === state.currency);
    const _price: string = coin ? coin[0].marketPrice : '0';

    if (text !== '.' && isNaN(text)) {
      const _input = state.input.substring(0, state.input.length - 1);
      // not a valid number
      setState({
        ...state,
        input: _input,
        amountInUSD: numeral(_input)._value * 0.0160178,
      });
      return;
    } else {
      setState({
        ...state,
        input: state.input + text,
        amountInUSD: numeral(state.input + text)._value * 0.0160178,
      });
    }
    // _input && _input.focus(); // focus to toggle the keyboard
  };
  
 const  sendrequesttransaction = () => {

fetch('http://34.77.195.149/transaction/request', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization':'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZTkwN2VkNjY0ZTA4OTAwMTAyNTI3ZDYiLCJpYXQiOjE1ODY1MzI0NDB9.iv_CHAcT6TfGjzg6ziaZaRBO8hrWlKyvYkG9ekwu-fk'
            },
            body: JSON.stringify({
                 from: "giraffe@example.com",
                  amount: state.amountInUSD,
                  currency: "php"
            })
        })

            .then((response) => response.json())
            .then((responseData) => {
                console.log("RESULTS HERE:", responseData)

        //     this.setState({
        //   isLoading: false,
        // }, function(){

        // });
      })
      .catch((error) =>{
        console.error(error);
      }) 
};

const sendtransaction = () => {

fetch('http://34.77.195.149/transaction/5e9054f7c7cb3014912c1da8/authorize', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                  amount: amountInUSD,
                  currency: "php"
            })
        })

            .then((response) => response.json())
            .then((responseData) => {
                console.log("RESULTS HERE:", responseData)

            this.setState({
          isLoading: false,
          dataSource: responseJson,
        }, function(){

        });
      })
      .catch((error) =>{
        console.error(error);
      }) 
};
  // call onSell function from the main component to handle the logic
  const _onSell = (): void => {
    if (state.amountInUSD > 0) {
      onSell(state);
      onClose();
    } else {
      Alert.alert('Error', 'Please select the asset and insert a valid amount to invest in! ');
    }
  };
  // call onBuy function from the main component to handle the logic
  const _onBuy = (): void => {
    if (state.amountInUSD > 0) {
      onBuy(state);
      onClose();
    } else {
      Alert.alert('Error', 'Please select the asset and insert a valid amount to invest in! ');
    }
  };
  // render picker only if it doesn't come from specfic coin route
  const renderPicker = (): React.ReactElement<any> => {
    return wallets.length > 1 ? (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
      
        {Platform.OS === 'android' && (
          <Icon
            name="downcircleo"
            type="AntDesign"
            style={{
              marginLeft: moderateScale(-55),
              marginRight: moderateScale(50),
              color: Colors.black,
              fontSize: scale(25),
            }}
          />
        )}
      </View>
    ) : null;
  };
  // dummy call for the onClose functions received from the main component
  const _onClose = (): void => onClose();
  // dummy rendering


   // const subTitle = `Introduce a friend to React Native Cryptostream App and you'll both get 100 Fake ETH, and 1000 Xia when purchasing the App.`;
// the body of the email or sms or social network post when sharing with the invites with friends
    const message = `Hi friend, i'm asking you to send monay on flamingo
     https://eu.jotform.com/form/200943494849366

    Thank you!`;

    const onShare = async (): Promise<any> =>
    await Share.share({
      message,
    }).catch((e): void => _showMessage(e.message));




  return (
    <View style={{ flex: 1 }}>
      <Header
        androidStatusBarColor={Colors.white}
        iosBarStyle={'light-content'}
        transparent
        translucent
        style={{ backgroundColor: Colors.trueBlue, borderBottomColor: Colors.trueBlue }}>
        {IS_iOS ? <Left /> : null}
        <Body>
          <Title style={{ color: Colors.white }}>{`Pay or Request `}</Title>
        </Body>
        <Right>
          <Button transparent onPress={_onClose}>
            <Text
              style={{
                color: Colors.white,
              }}>
              Cancel
            </Text>
          </Button>
        </Right>
      </Header>
      {wallets.length ? (
        <Content>
          <Form>
            {renderPicker()}

            <Item style={{ marginHorizontal: moderateScale(20) }}>
              <Input
                editable={false}
                style={{
                  fontSize: scale(18),
                  fontWeight: '600',
                }}
                autoFocus
                value={state.input}
                keyboardAppearance="dark"
                keyboardType="decimal-pad"
                placeholder="Insert amount..."
                // onChangeText={onInputTextChanged}
              />
              {state.icon ? (
                <Image source={{ uri: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABO1BMVEX///8AOKjOESbMzMz80Rb8zwDJycn80AD8zQAAOazTDx2GKHPLAADT0s7M0tLR0dH09PTs7OwAKaYAM6fX19f5+fnh4eEAL6fOAB/b29vn5+fV1M781Djj4+Pr6+vM1NP//fX/+ejOAA4AH6L95pL80yIAMKf+9tv82VL+8MH/++v955r95Y3OABT94HX++OH+7rr+883MrrHNV2BTarL93Wz+8MD93WX810JxgrivtMWfqMIrTqwAGqH+66r+6qfNdHrNaXDNh4yWnr9re7aAjbtfc7SXocDL0+mns9oSQazAyeX44uTggYjyzdDSMT/NmJzuur7NpKfOHjDORlE7V65IYrA1VK6rssW6vshwhMbZ3++Qn9F9j8rk6PSfrNaaY43lmJ7NX2fpqa7iipDOPEj22dvMu7zNjpI78+TjAAATKElEQVR4nO1dC1fbRhYWyJIcb2wZ+Q0OGDUGg7GxecQOsLwSyIbtFpaQNNlm0226Kf3/v2BnNDOyHiNpRk+yh++c5oDBrj7u47v3zowkCI94xCMe8YhHPATUyguL9Va1qoqiDCGKarXaqi8ulGtZX1pk1BbqgBem5QB6Va3WF75TnoCcSmVGY6p+bzRri1WRhZyNptha/k5YlusihZyqFu1QVQpN9Vk568sPQrnlpAeZqfLB4auLi5evL/cgLl+/vLh4dXggGz9zkBTrD5gktJ71covFJfng1cXe+zlN0xqNxtpaBWFtDXwHXpt7v3fx6kBeKhZtLB8qyUXVaj1w0QcXl1cNwKxSmfNCpQKYNq4uXx2IVpbAXZezpuNErWWhpxaXDi4+zGmNyhNPblY8qTS0uQ8XB0sWl5Xl+kPKO+WqlZ54+PoKsGMiZzFnQ7t6fSjOSMpy9aE464Iqz+iph3sVbY3Ndi5brmlzl4cWd5XVh8CxPONXLB7uaRqv8Rym1LTLjzOS2XO0+GdRvriKSI+QPLqQZxwz9dWayU9d+rinNcI5J4VjQ7s8WCIRKbcyyzl1k1/x8H0s5pthTftwaGYduZ4JvwUi72rxzVHM/CAq2s8mR1lcSJ2f6aAJ8UMcjw5JPMrVlF11kTjo0mFS/BDH9x+XSM5Js8ypEYUofvqQID/Ece9TkZgxNYILpoO+TJifwbFxYaaclISjJRMHvWokzg+icUVcNZWkWsYptCjvaXHpXxAq2qWo4hon8YSzTAz4prKWEj+IxtwhMWPCuoFFXhUvtRT5ATzRXhNtfJYkQSyCxYOUItCKxhFOqnIrMX41ooEXqUWgFZXGqyUcjAkRLBOV/5Cyh5rQLkmJk0i+wSpY/HSVZoqxo3Ek45yagDLiJLp0uJa8yHujMndQTCil4kJ06eKHDPnNwZz6ZikRioTgy6xCcAbtIgmKmGBxL3uCgOLL+CligvL79FWQBpJS46NIKrUv//prlllmhsaHYqwZlTRLQIN+ef/Dg+AYL8XyjCDA3+ceQiiaFOOQ/hqOQfOjPmvZKf4MjT1MMTpD0eUOXy4fQjhqr1FGjVyjUoukX37OWPcNikg0ok5v0MTCnZbfVrIPRyz90fpFJITyIuVHn3/IPBy1N8WosojSqEfH+WUvc+XQDlR7FuQGyjKejv63o4xd9cmcHC3bVAPz8du1bDlWfkZ+GnLIiIPQt2yo/Ttb5WggzQhX2yCpl4NWC758yDQctVfF0MKv+mQZG/5+lclgCqPxifU6nXgms/9tfm1k11ZVjkJKRo0hCGcoZ9hXNXBDzMtQ5cxRv2QXjtpHNYSfoqaXS2dAOGbDsHJVDJFPQ/WXn7VswhH7KZc9jIKbX0fL/8rGVbUDz/LZ60o58qgd2fRVlSPeZFONULJnMuZAus/udMiEoTvLz3/NoK+SuZoMNeIY68tl6uHYeG3kU0bFQNPDSMuQfztKOxy1TxxGFLkM7oG0xxwVNHtjCq3lcErhRO3f6Y45sGKwxBaKwqgEBTjmSLNYrbxfYgwuIwpj2u+Q6phD+8gYXawmHK6zcHybXiFX+cCmiWXGKBzlcx0WiiAc03JVHIlBV9RiM/W2lFOmLAxTHHNULpcYqtMamxZ2lFwuJw3ZKKbWV6GBRkCLYcwuglNuDzLMSceMFIVfUwnHxsti8NWzDVi38zmDIaOfCmn1VRX/CbZxITJTU9GRDIbKPjPDdJaPcYvhdxkthjZrBP7LIYZbwJwjZo7JhyNSfd9cwyAV6xL4p4289EQQNvIbzBSTH3NoQblmgSHPtPObZqYB9utLEgfFpKeOONd4i1012Em3JWkMChrEEHy1r0htdoZGOCbI8MnVkn/NyVKSdqU+tJzBcBV8m4Pf8yDR9SpU13i6KXLSgHpmCwr9scFQ6QhCPicxFahWJLibY+21r5u2HPz73TZATxBWt2YsxtArN5BcwLwD/7Vi23hTd9WPYnLLx0+M6bDngpnocNLnyBeBzU7ys2CTgOnWjZ+0oTEVUrttK93uROh0FeLAfkhszIHc1EP0y85MihgaKXOSN6Ntkt8WNuFPFGDdLUUiggjyq9TBWTaQIQzHRJSjceEj+mg9zfICZggjraNI2/jV4/wIhh9gOIS0ACsDL6Sc9AJEaY6RYUJ9FRoOeyie6mwrCMMcaAXHeVKEbudBKWP0Fs9BKjV7KMOdR+QdDAwT6qs0n6rFtahNDGK0glumn+YmgjBFrUVHIloB9KMLu0aMPAtDEI7xLx833qheelF2acW6ohCTDGExg/10S4FCbwj+WMJaAQITuDHhl5MmTAQB3sa9fIz1gvb/MrZe2Llvdk2KI+Cd2CHH+Q5gaYTniYS1ApQ2fZJlgAU5ioByzLs50Ko3NRCrtO6+ZwbWOkgx+MLzY0gNZs4h7vPHEkisZpZRxuwEhdjDEQUirb9whaHwogMqUEu26eHo6h0bkg+ajDbWirYiwWSKCHY3hTFbGBLE2ldphyp9EFNz9xVb0Bh9fOHASB2cOE96x4Zt948VyXjhGDQYq4QgCME+CVlmxDjmQP0FpX9ARantpecSFATTNkMQgcBPR9OcIqG6VMopkzGcS4GfEW/uC52pJPEyjHHMgVYwKKnmmTvRgMCSpptmSgVKvyX1c5Lpt4jldLwldUnHCPLrSFJy3DYU4htz4A7KnWpa7kSzngNGApGGa02gBxt2eiSzKKsoy0jtTWEf/FmY54x2xLR8bAwVKcW3SstAMM9I+7OUSuEH0cX94kRY70K7c/dTBLFsyzUWMChVDX1tagyvOLduSalUKDgE+/C32WdTbnyJIRwbxsTN1V7UvLrfYd6oQPt5X4o5o8SZSoq0FYEfRPS+as1Ipq66rexsLEwYZpx2NvytmGt3XgB+PT4hpOJtxHDEy8HOj13wZGhEo6IMFX+G3aEkdTnGbjTgP08tWjiijcMufzSqUq95ODRjAEEQiwrnSMqNHvki4vIxNacYB+2915yCLJjjWafxQr87+zrK8rFG3fDUcmbYvq18HgdEoYGIBDftk9fwU0eNKohVhxzuS5KyD0tvhDYDQbsR1495VaPnmC2HHnMgQXRqu2qnvW+MmiRpcrwJv91mMWGOXODqi+c98N58ryMEofPC/HIk5ZzT85B9ldFduCTf7rrj4RTUXqDABBc67W9bJja+RlwVOuP+RMkDgMvtDZ8HMhRG+SEOByXnYhiyr0KDDGdScfdOqxv9/W5egmh3WQjmctNuPq9Mh1vHG+udTjA7A/28pAw3UIFIWwH59QfuvgpNFP0Y9sEVbuIr3B4973WZLAgZbm9aPrKzOh71gxUSqq0kwcEPlaFQ4y7kEEOn9M0YdtqG3ZTudLLVH61DFWbKpJCh8f718XF/2Gt3FeNzXjiv2AWj8TLUqEsff/D2VQEMx+aADWg4vMS80p4y2lDZB/ZGfx/zQxhWUGc+AhyWSpIvHOmFKWF4wuqRdIq09jHQipuK7ROoJD83njBj7ZJWmGKGJ4EtBDekfCeI4rFkf4NCkdIvf2HHf0qUOQax4T7FCjTLeLChvLkbONFgseFveoEZ+u4KhaFZrE7MqzTDcNJjpKi4w1DqUS7XgeA4/Oc7fZ4dza8lipfOFH9iTaVjwwA8uZSk0hxKpQwNcWAu/fHuaZOD4HzzzJ+hTQ659FDqueSQoTYN1MN/8PEDDE9LlFzqqEuFWU0DypQea00j5dvD43FH4EBQTfPb0wEfv/n5ApWhvbfAdSlAdzhaF1jr0k1h/Tko3KRub2vEOG879q9Lf7rWefkBhucliuJX7Wv4+8B43ckJMcc2k4igmf/mCNTexh9nElx5+/cWvAGIMfizRKlLnR1wf8MaUwJLVWPpD8fPYek35RtLufpD7gAkDG9oDAOmGEGTNoiu7R2rx7xTDUeP/3uBOwAx9NsSZeeT7yRKYDNi1EmUdU7z0/XTkPwAw3taj+83TQR/3okSPGuLPk00q4Mfd0M6KGJI3SXrPREW4CKUFDgvzXVGcCIcYtFpBhK3/43CD4A6TfSc6oMkB4SDYea9KWzGMdUHATgfNgARmt9KVC70lRlBWJ9KEuu6BdRNSYk2N40SgJihUXi7/dFd1EB0hoCfufbk6ahk7UkYg/ZCavPtVLCi/DWig857lTRY8p0TuBNAb7Z+2N0KXD9E7Zc06YQj+NvTQlR+RA7dulB3y8V627YGDNyQllIl6xrwqhB6lRv0SPN6dH5ELNwbgZfdydS+jg9yyBh0QyDQbP4p9Wzr+EAvNsPsVBCEP+6iBiBGk7psQdv0Zd+LoUyFjrH7a9RTLHsx4BE2x14M8EZ+G5bPogcgJni94pE03TJp208DGocJ3mp50jsxAnM48thPw7tjCA4poimEleEZbUoDobq817onCoTjMdmIaN0ThZqkNuBu3RPFCb4hRQBQoqFtTmzRtg+bmzCGwqpEamvrvrYT45VI+9pC9kheQImGtsGUkmpmexO70F4SunLb3kR89CnC3sSwPZInPBINZZu3dX/pNrCQgjeN2vaX4oYn9P7S3wexBSBCc5des0GIzp7DFHjQ247z5s5m+x7hDnox3B7hn+IMQITBOW3QhlB1BiKZzgDjdQw9NGDf522eegqxzztaj+QBIww9evlF5+5Ek6EAhAL+a8C+V18isxj+vfqxByDCCn1Xm/G/dCoiOW8xhgsL5rkK+3mL2Uyb87zF782YAxABq6HHsSDRYV98ZqYvrCrdrsnEdmYGFKHkda4zM9F7JA8YMxrPo111h16gc0+OtOg89+QsQVnOPSUSgBgivexGcOsFBc6zaxL/VsSoQwofoEUZbw6iVzVggfP8IffY4p/JBCACclLvQ6IMB51dZ0j5jpAKf3xLKAARCiV/I7G4qfMcME+FFsuQwg84k/owcGZTChxnuV90fX/bjt/0GIYUftB3gryw7j6S4ILjPD77nu6YhhQ+aF6X/DIpRNnzRI0NYe6p8Ee8PRIVqCb1P8lsnEFM4L4YSZVodugM97NcTObeJjEOKXzQPPOpSU2w3W6P7/40sQ4pfIDaiqC7J9bFQE+G4LjHUMxDCm+gaX7gTbwYbzLEfp+oVALQgL4TnGcgqmxGZLzX1++pBKABtOTEYBzWm32xILEeiQZUkrLcdC/qzfhNJNkjudH8tsKUJAVy1DL6fRMT7JFowFHIdE/SWIyY0JDCE827EqsJyaaFSA83STUADSAtZL2tbNR70P6YcI9EQeGMOQohyvy3K7ciloVcTjT57iOMNTHcU6KT75EowE0F+52B8WMDQvBLeEjhAdQXcmVH1Alz3yw5voVcPmCl4LqtrBgm2WQRgBA4zfDdQ36Z/77zafVIbrwLZY8qy+zUgtR6JDfwjJQ3pjifUZJej+TC4DSEj0Iscvhp7Au5HCB5NIS2oafmMVQJCSzkcmBwz3qdLtS8nypnBeiRMuQ3rxu71kM+6JHpmV0ZBiDEAAlF2MfnVQNDMe0eyQk8uQjfzRoEvUvw9HskFyI+O8//+YfpDimoQNVapG7d5xmWKQ8paHh6U4oShAgtjwfoZR2AEPo5yjIRn+NAfZZsNj2SkyCqZaI+S5b2POAMhhQUYJ2I/jxg9zOdE1/IZcIAHTmI45nOjudyZzKkcIMQjGNybXu2emy7zSNisFsK1RN6AbcZ4o8ZDSlcMAlG0gkr8BM75bvsJQJCP4uboGnF0q6eNbt5UyZiJWhSXDnVs+ZHhD5mgqajrpxnnGqa+m0yBGdWvC1kmW2a8/fxxyABFo3S/bvsKA6+yfHKhB1Y+kvynZ4RQZJESfkRO2r44zPKN83BDQ7B6LWoJ1QcjDvz6demg2sSgpEePhmEFvZUMXVl1E+xh8b0XD9PkApu5SbVnFp4t4N7iSSSqB1lkSSc9MzY1M9E7KFqQjnGBjxkFEs3KUXj4No0YDw7mQKxTCiKp3ryrtpsnpeISCSigjTUVMLx/i5hjk39q0xSTOSJDA9wmQqruDB3AWLnd0ccVBTD7ZwIjZkZSzfvErJjU/92S/wzWRGkY1mccbxOgGPz6d3tCnHQ9CLQhhZxVcDxW8wcm/rujplgkhZ5b5SrM447u/HtxGgOBmf3ZvzJrTQ00JOjOuN4fxpPQDb16z/FGb9qlvwgFkyOgOTtrj6IRrKpF85m7gn4ZROAdlg5rog3u83QJAv6/NntjN4D4Qcx81VoSPnmbF7nrsubA/3d6Y64MjOf3Hoo/CDKLdlKsrRzetdkZwnZ7Z7fr1jNJ9azjj8naovijCNkKe6c774b6INC05tos1kY6IXrsz/vRQs7YD415QKGEeWWjSSIypX7nT9Pd7/NP9UHg0IBcMUoFAYDXdff3X09v9mRrbaD9MT6Q3JPBxZaomxjCWwDQkveub05Pz09+7oL8fX09Pz85nZHBj+ykXvw9BDKdSdJHJsQKxD4a9fvAOd89uDpIdQWqzSWPpDBr7eWH1pu8Udtua7KMgNP+EvV+sL3xc5EebleNXhSmKJX1eqz75WcBbXywmK9Va2qxq2oRFFV1Wq1VV9cKH//3B7xiEc84hH/H/gfmq6IcF212fsAAAAASUVORK5CYII=' }} style={{ height: 40, width: 40 }} />
              ) : (
                <Icon active name="money-off" type="MaterialIcons" />
              )}
            </Item>
            <Item style={{ marginHorizontal: moderateScale(20) }}>
              <Input
                style={{
                  color: Colors.trueBlue,
                  fontSize: scale(18),
                  fontWeight: '600',
                }}
                editable={false}
                placeholder="£ 0.0"
                value={numeral(state.amountInUSD).format('£ 0,0[.]0[0000]')}
              />
              <Icon active name="swap-vert" type="MaterialIcons" />
            </Item>
            <View style={styles.buttonContainer}>
             
              <Button onPress={sendrequesttransaction} style={styles.button} block>
                <Text style={styles.buttonText}>Request</Text>
              </Button>
            </View>
          </Form>
        </Content>
      ) : (
        <LoadingSpinner />
      )}
      <Keyboard inputChange={onInputTextChanged} />
    </View>
  );
};
const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: verticalScale(10),
  },
  button: { flex: 1, marginHorizontal: moderateScale(10), backgroundColor: Colors.trueBlue },
  buttonText: {
    color: Colors.white,
    fontWeight: '600',
    alignSelf: 'center',
    fontSize: scale(13),
  },
});
export default Invest;
