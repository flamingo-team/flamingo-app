import numeral from 'numeral';
import React, { useRef } from 'react';
import { StyleSheet, View, Text, ART, TextInput } from 'react-native';
import { moderateScale, scale, verticalScale } from 'react-native-size-matters';
import {
  State,
  GestureHandlerStateChangeNativeEvent,
  PanGestureHandlerEventExtra,
  TapGestureHandlerEventExtra,
  LongPressGestureHandlerEventExtra,
  RotationGestureHandlerEventExtra,
  FlingGestureHandlerEventExtra,
  PinchGestureHandlerEventExtra,
  ForceTouchGestureHandlerEventExtra,
  PanGestureHandler,
  TapGestureHandler,
} from 'react-native-gesture-handler';
import Animated from 'react-native-reanimated';
import { IChartProps } from '../store/interfaces';
import { Colors, Screen, Ranges } from '../enums';
const { Surface, Group, Shape } = ART;
// the cursor radius
const cursorRadius = 10;
// reanimated props
const { Value, event, block, set, eq, call, cond, Extrapolate, interpolate } = Animated;

export default function Chart(props: IChartProps): React.ReactElement | any {
  // dummy rendering and calls. all the logic is in the components under screens folder (screens/)
  //create refrence to the InputText to maniplulate programmatically
  const label = React.useRef<TextInput>();
  const time = React.useRef<TextInput>();
  const cursorLine = React.useRef<View>();
  // extract components props
  const {
    fillColor,
    strokeColor,
    strokeWidth,
    coin: { change, price },
    values,
    range,
    withHeader,
  } = props;

  // get path and cursors
  const { path, cursors } = props.buildPath(Screen.chartHeight, Screen.width, strokeWidth);
  /**
   * reanimated gestures
   */
  const cursorOpacity = useRef(new Value(0)).current;
  const coinValueOpacity = useRef(new Value(1)).current;

  type NativeEvent = GestureHandlerStateChangeNativeEvent &
    (
      | PanGestureHandlerEventExtra
      | TapGestureHandlerEventExtra
      | LongPressGestureHandlerEventExtra
      | RotationGestureHandlerEventExtra
      | FlingGestureHandlerEventExtra
      | PinchGestureHandlerEventExtra
      | ForceTouchGestureHandlerEventExtra
    );

  type Adaptable<T> = { [P in keyof T]: Animated.Adaptable<T[P]> };

  const onGestureEvent = (nativeEvent: Partial<Adaptable<NativeEvent>>): object | any => {
    const gestureEvent = event([{ nativeEvent }]);
    const stateChanged = event([
      {
        ...nativeEvent,
        nativeEvent: ({ state }): any =>
          block([
            cond(
              eq(state, State.ACTIVE),
              [set(cursorOpacity, 1), set(coinValueOpacity, 0)],
              [set(cursorOpacity, 0), set(coinValueOpacity, 1)],
            ),
            cond(eq(state, State.END), [set(cursorOpacity, 0), set(coinValueOpacity, 1)]),
          ]),
      },
    ]);
    return {
      onHandlerStateChange: stateChanged,
      onGestureEvent: gestureEvent,
      cursorOpacity,
      coinValueOpacity,
    };
  };

  const panGestureHandler = (): any => {
    const x = new Value(0);
    const state = new Value(State.UNDETERMINED);
    const gestureHandler = onGestureEvent({
      x,
      state,
    });
    return {
      x,
      gestureHandler,
      state,
    };
  };
  let { gestureHandler, x } = panGestureHandler();

  const inputRange = cursors.map((_): number => _.x);
  const translateX = interpolate(x, {
    // we need to interpolate here so the output should take in considertation the cursor radius to center it
    inputRange: cursors.length ? inputRange : [0, 1],
    outputRange: cursors.length ? cursors.map((_): number => _.x - cursorRadius) : [0, 1],
    extrapolate: Extrapolate.CLAMP,
  });
  const translateLineX = interpolate(x, {
    // we need to interpolate here so the output should take in considertation the cursor radius to center it
    inputRange: cursors.length ? inputRange : [0, 1],
    outputRange: cursors.length
      ? cursors.map((_): number => _.x - moderateScale(StyleSheet.hairlineWidth) / 2)
      : [0, 1],
    extrapolate: Extrapolate.CLAMP,
  });
  const translateY = interpolate(x, {
    // we to interpolate to get the y output and use x interpolation bcuz we gesture is sliding right & left on x axis
    inputRange: cursors.length ? inputRange : [0, 1],
    outputRange: cursors.length ? cursors.map((_): number => _.y - cursorRadius) : [0, 1],
    extrapolate: Extrapolate.CLAMP,
  });

  const __label = interpolate(x, {
    // interpolate x to get the price to update later when moving the cursor:: see the listener down below
    inputRange: cursors.length ? inputRange : [0, 1],
    outputRange: cursors.length ? cursors.map((_): number => _.price) : [0, 1],
    extrapolate: Extrapolate.CLAMP,
  });

  const __time = interpolate(x, {
    // interpolate x to get the price to update later when moving the cursor:: see the listener down below
    inputRange: cursors.length ? inputRange : [0, 1],
    outputRange: cursors.length ? cursors.map((_): number => _.time) : [0, 1],
    extrapolate: Extrapolate.CLAMP,
  });
  // render prices when moving cursor
  const renderPrice = ([__label__, __time__]: [any, any]): void => {
    label.current &&
      label.current.setNativeProps({
        text:
          numeral(__label__)._value < 10
            ? numeral(__label__).format('$0,0[.]0[000]')
            : numeral(__label__).format('$0,0[.]0[0]'),
      });
    time.current &&
      time.current.setNativeProps({
        text: `${new Date(Number(__time__) * 1000).toDateString() +
          ' ' +
          new Date(Number(__time__) * 1000).toLocaleTimeString()}`,
      });
  };

  const tapGesture = event([
    {
      nativeEvent: ({ state, x: X }): any =>
        block([
          cond(eq(state, State.BEGAN), [set(cursorOpacity, 1), set(coinValueOpacity, 0), set(x, X)]),
          cond(eq(state, State.END), [set(cursorOpacity, 0), set(coinValueOpacity, 1)]),
        ]),
    },
  ]);

  //* default rendering
  return (
    <View>
      {withHeader ? (
        <Animated.View style={{ ...styles.header, opacity: coinValueOpacity }}>
          <View style={styles.left}>
            <Text style={styles.price}>{price ? numeral(price).format('$0,0[.]0[0000]') : '—'}</Text>
            <View style={styles.row}>
              <Text
                style={[
                  styles.change,
                  {
                    color: change >= 0 ? Colors.green : Colors.red,
                  },
                ]}>
                {change ? (change < 0 ? `▼${Math.abs(change)}%` : `▲${Math.abs(change)}%`) : '—'}
              </Text>
              <Text
                style={[
                  styles.change,
                  {
                    color: Colors.black,
                    paddingHorizontal: scale(10),
                  },
                ]}>{`${
                Object.keys(Ranges)[Object.values(Ranges).indexOf(range)].toLowerCase() === 'all' ? 'for' : 'this'
              } ${Object.keys(Ranges)[Object.values(Ranges).indexOf(range)].toLowerCase()}`}</Text>
            </View>
          </View>
          <View
            style={{
              height: Screen.chartHeaderHeight - verticalScale(15),
              width: moderateScale(1.5),
              backgroundColor: '#ccc',
            }}
          />
          <View style={styles.right}>
            <View style={styles.row}>
              <Text
                style={[
                  styles.change,
                  {
                    fontWeight: 'bold',
                    color: Colors.green,
                    paddingHorizontal: scale(10),
                  },
                ]}>{`▲`}</Text>
              <Text style={styles.change}>{`${
                values.length
                  ? numeral(Math.max(...cursors.map((o: any): string => o.price))).format('$0,0[.]0[0000]')
                  : '—'
              }`}</Text>
            </View>
            <View style={[styles.row, { justifyContent: 'flex-end' }]}>
              <Text
                style={[
                  styles.change,
                  {
                    fontWeight: 'bold',
                    color: Colors.red,
                    paddingHorizontal: scale(10),
                  },
                ]}>
                {`▼`}
              </Text>
              <Text style={styles.change}>{`${
                values.length
                  ? numeral(Math.min(...cursors.map((o: any): string => o.price))).format('$0,0[.]0[0000]')
                  : '—'
              }`}</Text>
            </View>
          </View>
        </Animated.View>
      ) : null}
      <Animated.View
        style={[styles.header, { flexDirection: 'column', justifyContent: 'center', opacity: cursorOpacity }]}>
        <Animated.Code>{(): any => call([__label, __time], renderPrice)}</Animated.Code>
        <TextInput
          style={[styles.price, { color: Colors.trueBlue, textAlign: 'center' }]}
          editable={false}
          selectTextOnFocus={false}
          ref={label}
        />
        <TextInput
          style={[styles.change, { color: Colors.black, textAlign: 'center' }]}
          editable={false}
          selectTextOnFocus={false}
          ref={time}
        />
      </Animated.View>
      <PanGestureHandler
        onGestureEvent={gestureHandler.onGestureEvent}
        onHandlerStateChange={gestureHandler.onHandlerStateChange}>
        <Animated.View style={{ height: Screen.chartHeight, width: Screen.width, marginTop: Screen.chartHeaderHeight }}>
          <TapGestureHandler onHandlerStateChange={tapGesture}>
            <Animated.View>
              <Surface width={Screen.width} height={Screen.chartHeight}>
                <Group x={0} y={Screen.chartHeight}>
                  <Shape
                    d={path}
                    fill={fillColor}
                    stroke={strokeColor}
                    strokeWidth={strokeWidth}
                    // opacity={1}
                  />
                </Group>
              </Surface>
              <Animated.View
                ref={cursorLine}
                style={{
                  position: 'absolute',
                  height: Screen.chartHeight,
                  backgroundColor: Colors.trueBlue,
                  width: moderateScale(StyleSheet.hairlineWidth),
                  marginRight: moderateScale(-cursorRadius),
                  transform: [{ translateX: translateLineX }],
                  opacity: cursorOpacity,
                }}></Animated.View>

              <Animated.View
                style={[styles.cursor, { opacity: cursorOpacity, transform: [{ translateX }, { translateY }] }]}
              />
            </Animated.View>
          </TapGestureHandler>
        </Animated.View>
      </PanGestureHandler>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.transparent,
    height: Screen.height / 2,
    zIndex: 999,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    position: 'absolute',
    width: Screen.width,
    height: Screen.chartHeaderHeight,
  },
  right: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: scale(15),
  },
  left: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingHorizontal: scale(15),
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  price: {
    fontSize: scale(22),
    color: Colors.black,
    fontWeight: '600',
  },
  change: {
    fontSize: scale(15),
  },
  scroll: {
    height: Screen.height,
    width: Screen.width,
    backgroundColor: Colors.transparent,
  },
  cursor: {
    width: moderateScale(cursorRadius * 2),
    height: moderateScale(cursorRadius * 2),
    borderRadius: moderateScale(cursorRadius),
    borderColor: Colors.trueBlue,
    borderWidth: moderateScale(cursorRadius / 2),
    backgroundColor: Colors.lightGray,
  },
  cursorText: {
    color: Colors.grayish,
    fontSize: scale(12),
    marginLeft: -20,
    marginTop: moderateScale(-(cursorRadius * 3.75)),
  },
});
