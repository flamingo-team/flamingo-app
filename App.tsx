import React from 'react';
import { View, Image, Alert, StatusBar, Platform } from 'react-native';
import { StoreProvider } from './store';
import Navigator from './navigation/Navigator';
import FlashMessage from 'react-native-flash-message';
import { Colors } from './enums';
import { AppLoading, SplashScreen } from 'expo';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import * as Icon from '@expo/vector-icons';
// react-navigation-screens is important for react-navigation navigation performance
import { useScreens } from 'react-native-screens';
useScreens();

export default function App(): React.ReactElement {
  SplashScreen.preventAutoHide();
  const [state, setState] = React.useState({
    isSplashReady: false,
    isAppReady: false,
  });
  const _cacheSplashResourcesAsync = async (): Promise<any> => {
    // you can add gif image as splash if you wish to have an animated splash
    const splash = require('./assets/splash.png');
    return Asset.fromModule(splash).downloadAsync();
  };

  const loadRecources = (): void => {
    _cacheResourcesAsync().then((): void => {
      setState({ ...state, isAppReady: true });
      SplashScreen.hide();
    });
  };
  const _cacheResourcesAsync = async (): Promise<any> => {
    const images = [
      require('./assets/coinbase.png'),
      //add images you would like to cache in preloading.
    ];
    const fonts = [
      {
        ...Icon.Ionicons.font,
        Roboto: require('native-base/Fonts/Roboto.ttf'),
        Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
        // Smiley: require("../assets/fonts/smilf___.ttf")
      },
    ];
    const cacheFonts = fonts.map((font): any => Font.loadAsync(font));
    const cacheImages = images.map((image): any => {
      if (typeof image === 'string') {
        //return Image.prefetch(image);
      } else {
        return Asset.fromModule(image).downloadAsync();
      }
    });

    await Promise.all([...cacheImages, ...cacheFonts]);
  };

  const _handleLoadingError = (err): void => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    Alert.alert('Error', err.message);
  };
  // UIManager is to enable animation on android
  React.useEffect((): void => {
    // if (Platform.OS === 'android') {
    //   // if (UIManager.setLayoutAnimationEnabledExperimental) {
    //   //   UIManager.setLayoutAnimationEnabledExperimental(true);
    //   // }
    // }
  }, []);
  if (!state.isSplashReady) {
    return (
      <AppLoading
        startAsync={_cacheSplashResourcesAsync}
        onFinish={(): void => setState({ ...state, isSplashReady: true })}
        onError={_handleLoadingError}
        autoHideSplash={false}
      />
    );
  }
  if (!state.isAppReady) {
    StatusBar.setBarStyle('light-content');
    Platform.OS === 'android' && StatusBar.setBackgroundColor(Colors.white);
    return (
      <View style={{ flex: 1, backgroundColor: Colors.trueBlue }}>
        <Image source={require('./assets/splash.png')} onLoad={loadRecources} fadeDuration={0} resizeMode={'cover'} />
      </View>
    );
  }
  // default return
  return (
    <StoreProvider>
      <Navigator />
      <FlashMessage position="top" />
    </StoreProvider>
  );
}
