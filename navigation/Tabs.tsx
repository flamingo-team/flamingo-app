import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { moderateScale } from 'react-native-size-matters';
//local imports
import { ITabBarIcon } from '../store/interfaces';
import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/Home';
import AssetsScreen from '../screens/Assets';
import NewsScreen from '../screens/Stories';
import AccountsScreen from '../screens/Accounts';
import CoinScreen from '../screens/Coin';
import InviteScreen from '../screens/Invite';
import Transact from '../screens/Transact';
import Prices from '../screens/Prices';
import { Colors, IS_ANDROID } from '../enums';
// HomeStack represents 4 screens as stack navigator: Home, Coin, Asssets, Stories
const HomeStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: (): object => ({
        header: IS_ANDROID ? null : undefined,
        headerTintColor: Colors.white,
        headerStyle: {
          backgroundColor: Colors.trueBlue,
          shadowRadius: 0,
          borderWidth: 0,
          borderBottomWidth: 0,
          shadowOffset: {
            height: 0,
            width: 0,
          },
          shadowColor: Colors.transparent,
        },
      }),
    },
    Coin: {
      screen: CoinScreen,
      navigationOptions: (): object => ({
        header: null,
      }),
    },
    Transact: {
      screen: Transact,
      navigationOptions: (): object => ({
        header: null,
        tabBarOptions: {
          showLable: false,
        },
      }),
    },
    Assets: {
      screen: AssetsScreen,
    },
    Stories: {
      screen: NewsScreen,
    },
  },
  { mode: 'card' },
);
// HomeStack navigation configurations
HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  title: 'Home',
  tabBarIcon: ({ focused }: ITabBarIcon): React.ReactElement => (
    <TabBarIcon focused={focused} name={'home'} type="Feather" />
  ),
};
// AccountsStack represents Accounts Screen
const AccountsStack = createStackNavigator(
  {
    Accounts: {
      screen: AccountsScreen,
      navigationOptions: (): object => ({
        header: null,
      }),
    },
    Coin: {
      screen: CoinScreen,
      navigationOptions: (): object => ({
        header: null,
      }),
    },
    Transact: {
      screen: Transact,
      navigationOptions: (): object => ({
        header: null,
        tabBarOptions: {
          showLable: false,
        },
      }),
    },
  },
  { mode: 'card' },
);
// AccountsStack navigation configurations
AccountsStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }: ITabBarIcon): React.ReactElement => (
    <TabBarIcon focused={focused} type={'Feather'} name={'pie-chart'} />
  ),
};
// AccountsStack represents Accounts Screen
const OpStack = createStackNavigator(
  {
    Transact: {
      screen: Transact,
      navigationOptions: (): object => ({
        header: null,
        tabBarOptions: {
          showLable: false,
        },
      }),
    },
  },
  { mode: 'modal' },
);
// AccountsStack navigation configurations
OpStack.navigationOptions = {
  header: null,
  tabBarLabel: ' ',
  tabBarIcon: (): React.ReactElement => (
    <TabBarIcon
      withStyle
      focused={false}
      type={'AntDesign'}
      name={'swap'}
      style={{
        backgroundColor: Colors.green,
        height: moderateScale(36),
        width: moderateScale(36),
        borderRadius: moderateScale(18),
        marginTop: moderateScale(-7),
        justifyContent: 'center',
        alignItems: 'center',
      }}
    />
  ),
};
//prices Stack
const PricesStack = createStackNavigator({
  Prices: {
    screen: Prices,
    navigationOptions: (): object => ({
      header: null,
    }),
  },
  Coin: {
    screen: CoinScreen,
    navigationOptions: (): object => ({
      header: null,
    }),
  },
  Transact: {
    screen: Transact,
    navigationOptions: (): object => ({
      header: null,
      tabBarOptions: {
        showLable: false,
      },
    }),
  },
});
// prices navigation configurations
PricesStack.navigationOptions = {
  tabBarLabel: 'Prices',
  tabBarIcon: ({ focused }: ITabBarIcon): React.ReactElement => (
    <TabBarIcon focused={focused} name={'bar-chart-2'} type={'Feather'} />
  ),
};
//Invite Stack
const InviteStack = createStackNavigator({
  Invite: InviteScreen,
});
// InviteStack navigation configurations
InviteStack.navigationOptions = {
  tabBarLabel: 'Invite',
  tabBarIcon: ({ focused }: ITabBarIcon): React.ReactElement => (
    <TabBarIcon focused={focused} name={'gift'} type={'Octicons'} />
  ),
};
// creating the bottom tabs using createBottomTabNavigator from react-navigation package
export default createMaterialBottomTabNavigator(
  {
    AccountsStack,
    OpStack,
    PricesStack
  },
  {
    initialRouteName: 'AccountsStack',
    activeColor: Colors.trueBlue,
    inactiveColor: Colors.grayish + '80',
    barStyle: { backgroundColor: Colors.white },
  },
);
